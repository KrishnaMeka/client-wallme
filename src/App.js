import React from "react";
import "./App.css";
import { BrowserRouter } from "react-router-dom";
import AppRouter from "./routes/router";
import Authenticate from "../src/components/auth/auth";

const App = () => {

  return (
    <div className="App">
      <BrowserRouter>
        <Authenticate>
          <AppRouter />
        </Authenticate>
      </BrowserRouter>
    </div >
  );
};

export default App;
