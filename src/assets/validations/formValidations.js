const regex = {
    email: new RegExp(/\S+@\S+\.\S+/),
    number: new RegExp('^[0-9]+$'),
    name: new RegExp(/^[A-Za-z ]+$/)
};

export default function formValidation(fields) {
    console.log("passMatchMain", fields)
    let errors = {};
    if (fields.firstName) {
        if (!fields.firstName.trim()) {
            errors.firstName = 'First Name is required';
        }
        else if (!regex.name.test(fields.firstName)) {
            errors.firstName = 'Name must be characters only';
        }
    }
    if (fields.lastName) {
        if (!fields.lastName.trim()) {
            errors.lastName = 'Last Name is required';
        }
        else if (!regex.name.test(fields.lastName)) {
            errors.lastName = 'Name must be characters only';
        }
    }
    if (fields.email) {
        if (!fields.email) {
            errors.email = 'Email required';
        } else if (!regex.email.test(fields.email)) {
            errors.email = 'Email address is invalid';
        }
    }
    if (fields.password) {
        if (!fields.password) {
            errors.password = 'Password is required';
        } else if (fields.password.length < 5) {
            errors.password = 'Password needs to be 5 characters or more';
        }
    }
    if (fields.confirmPassword) {
        if (!fields.confirmPassword) {
            errors.confirmPassword = 'Confirm Password is required';
        } else if (fields.confirmPassword !== fields.password) {
            console.log("passMatch", fields.confirmPassword, fields.password)
            errors.confirmPassword = 'Passwords do not match';
        }
    }
    return errors;
}