import React from 'react';
import './inputField.scss';

const InputField = (props) => {
    return (
        <>
            {props.type === "file" &&
                <div className="moments-fileUpload">
                    <input
                        type="file"
                        className={`moments-inputFile ${props.styleName}`}
                        onChange={props.fileHandler}
                    />
                </div>
            }
            {props.type === "textarea" &&
                <textarea
                    value={props.value}
                    placeholder={props.placeholder}
                    className={`moments-inputField ${props.styleName}`}
                    onChange={props.onChange}
                />
            }
            {props.type === "text" &&
                <input
                    type="text"
                    value={props.value}
                    placeholder={props.placeholder}
                    className={`moments-inputField ${props.styleName}`}
                    onChange={props.onChange}
                    onKeyPress={props.onKeyPress}
                />
            }
        </>
    );
}

export default InputField;