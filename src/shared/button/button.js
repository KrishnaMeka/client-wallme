import React from 'react';
import './button.scss'

const Button = (props) => {
    return (
        <button
            type={props.type === 'submit' ? 'submit' : 'button'}
            disabled={props.disabled}
            className={`btn-default ${props.styleName}`}
            onClick={props.clickHandler}>
            {props.value}
        </button>
    );
}

export default Button;