import React from 'react';
import { useSelector } from 'react-redux';
import './spinner.scss'

const Spinner = () => {
    const spinner = useSelector((state) => state.globalSpinner)
    console.log("globalSpinner", spinner)
    if (!spinner) {
        return null;
    }
    return (
        <div className="spinnerOverlay">
            <div className="loader"></div>
        </div>
    )
}

export default Spinner;