import React, { PureComponent } from "react";
import ReactCrop from "react-image-crop";
import InputField from "../../shared/inputField/inputField";
import Button from "../../shared/button/button";
import person from "../../assets/images/person.png";
import "react-image-crop/dist/ReactCrop.css";
import "./pictureUpload.scss";

class PictureUpload extends PureComponent {
  state = {
    src: null,
    crop: {
      unit: "%",
      width: 30,
      aspect: 1 / 1,
    },
    croppedImageFile: null,
    croppedImageUrl: null,
    fileName: "",
    base64data: null,
  };

  onSelectFile = (e) => {
    if (e.target.files && e.target.files.length > 0) {
      const targetFileName = e.target.files[0].name;
      const reader = new FileReader();
      reader.addEventListener("load", () =>
        this.setState({ src: reader.result, fileName: targetFileName })
      );
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  // If you setState the crop in here you should return false.
  onImageLoaded = (image) => {
    this.imageRef = image;
  };

  onCropComplete = (crop) => {
    this.makeClientCrop(crop);
  };

  onCropChange = (crop, percentCrop) => {
    this.setState({ crop });
  };

  async makeClientCrop(crop) {
    if (this.imageRef && crop.width && crop.height) {
      const croppedImageUrl = await this.getCroppedImg(
        this.imageRef,
        crop,
        this.state.fileName
      );
      this.getCroppedImgFile(
        croppedImageUrl,
        this.imageRef,
        crop,
        this.state.fileName
      );
    }
  }

  getCroppedImg(image, crop, fileName) {
    const canvas = this.createCanvas(image, crop);
    return new Promise((resolve, reject) => {
      canvas.toBlob((blob) => {
        if (!blob) {
          console.error("Canvas is empty");
          return;
        }
        blob.name = fileName;
        window.URL.revokeObjectURL(this.fileUrl);
        this.fileUrl = window.URL.createObjectURL(blob);
        resolve(this.fileUrl);
      }, "image/jpeg");
    });
  }

  getCroppedImgFile = (croppedImageUrl, image, crop, fileName) => {
    const canvas = this.createCanvas(image, crop);
    const reader = new FileReader();
    canvas.toBlob((blob) => {
      reader.readAsDataURL(blob);
      reader.onloadend = () => {
        const base64data = reader.result;
        const croppedImageFile = this.dataURLtoFile(reader.result, fileName);
        this.setState({ croppedImageUrl, croppedImageFile, base64data });
      };
    });
  };

  createCanvas = (image, crop) => {
    const canvas = document.createElement("canvas");
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width;
    canvas.height = crop.height;
    const ctx = canvas.getContext("2d");

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    );
    return canvas;
  };

  dataURLtoFile = (dataurl, filename) => {
    let arr = dataurl.split(","),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);

    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    let croppedImage = new File([u8arr], filename, { type: mime });
    return croppedImage;
  };

  saveImageHandler = () => {
    if (this.state.base64data && this.state.croppedImageFile) {
      this.props.imageFiles(
        this.state.croppedImageFile,
        this.state.base64data
      );
    }
  };

  closeImageUploader = () => {
    this.props.closeUploader();
  };

  render() {
    const { crop, base64data, src } = this.state;
    return (
      <div
        className={`${this.props.createAccount
          ? "createAccountUpload modalPopupWrapper"
          : "modalPopupWrapper"
          }`}
      >
        <div className="modalPopupDialog">
          <div className="cropImageWrapper">
            <span className="closeIcon" onClick={this.closeImageUploader} />
            <div className="imageUploadHeader">
              Please uplaod a profile picture
            </div>
            <div className="cropImageUpload">
              <InputField type="file" fileHandler={this.onSelectFile} />
            </div>
            <div className="cropImageSection">
              <div>
                <span className="imageTitle">Original Image</span>
                <div className="originalImage">
                  {src ? (
                    <ReactCrop
                      src={src ? src : person}
                      crop={crop}
                      ruleOfThirds
                      onImageLoaded={this.onImageLoaded}
                      onComplete={this.onCropComplete}
                      onChange={this.onCropChange}
                    />
                  ) : (
                      <img
                        alt="Crop"
                        style={{ height: "102%" }}
                        className="croppedImageElem"
                        src={person}
                      />
                    )}
                </div>
              </div>
              <div>
                <span className="imageTitle">Preview Image</span>
                <div className="croppedImage">
                  {
                    <img
                      alt="Crop"
                      className="croppedImageElem"
                      src={base64data ? base64data : person}
                    />
                  }
                </div>
              </div>
            </div>
            <div className="cropImageBtns">
              <Button value="Save" clickHandler={this.saveImageHandler} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PictureUpload;
