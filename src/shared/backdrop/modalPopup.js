import React from 'react';
import './modalPopup.scss';

const ModalPopup = (props) => {
    return (
        <div className="modalPopupWrapper">
            <div className="modalPopupDialog">
                <div className="modalPopupContent">
                        {props.children}
                </div>
            </div>

        </div>
    )
}

export default ModalPopup;