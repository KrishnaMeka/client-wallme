import React from 'react';
import './formFields.scss';

const FormFields = (props) => {
    return (
        <>
            <div className="moments-formField">
                <input
                    type={props.type}
                    disabled={props.disabled}
                    placeholder=" "
                    value={props.value}
                    name={props.name}
                    autoComplete="off"
                    className={`formField ${props.className}`}
                    onChange={(e) => props.formHandler(e.target.value, e.target.name)}
                />
                <label htmlFor={props.name} className="formLabel">{props.label}</label>
                <span className="error-field">{props.error}</span>
            </div>
        </>
    );
}

export default FormFields;