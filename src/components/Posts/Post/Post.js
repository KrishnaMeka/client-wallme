import React, { useState } from "react";
import moment from "moment";
import Input from '../../../shared/inputField/inputField';
import Button from '../../../shared/button/button';
import Like_filled from "../../../assets/images/like_filled.svg";
import Like from '../../../assets/images/like.svg';
import More from "../../../assets/images/more.svg";
import Trash from "../../../assets/images/trash.svg";
import Edit from "../../../assets/images/edit.svg";
import Chat from '../../../assets/images/chat.svg';
import { useDispatch, useSelector } from 'react-redux';
import { startSpinner } from '../../../redux/actions/spinnerAction';
import { deletePost, likePost, commentPost, replyPost } from '../../../redux/actions/posts';


const Post = ({ post }) => {

  const [options, showOptions] = useState(false);
  const [showComments, toggleComments] = useState(true);
  const [comment, handleComment] = useState('');
  const [replyInput, addReplyInput] = useState({});
  const user = useSelector((state) => state.userInfo);
  const dispatch = useDispatch();

  const commentHandler = () => {
    if (comment.length > 0) {
      let commentObj = {};
      commentObj.comment = comment;
      commentObj.user = user.lastName;
      commentObj.postId = post._id;
      handleComment('');
      toggleComments(true);
      dispatch(startSpinner());
      dispatch(commentPost(commentObj));
      const divElem = document.getElementsByClassName("comments-wrapper")[0];
      divElem.scrollTop = divElem.scrollHeight;
    }
  }

  const getDateFormat = (val) => {
    const date = moment(val).fromNow(true);
    let days;
    if (date.indexOf('a') > -1) {
      days = date
    } else {
      const daysFormat = date.split(' ')[1];
      if (daysFormat === "seconds") {
        days = 'sec'
      } else if (daysFormat === "minutes") {
        days = 'min'
      } else if (daysFormat === "hours") {
        days = 'hrs'
      } else {
        days = daysFormat
      }
      days = `${date.split(' ')[0]}${days}`
    }
    return days
  }

  const replyEnterHandler = (name, id, key) => {
    if (replyInput[id] !== "") {
      const replyText = `${name}, ${replyInput[id]}`;
      let replyObj = {};
      replyObj.reply = replyText;
      replyObj.user = user.lastName;
      replyObj.commentId = id;
      const { [id]: temp, ...rest } = replyInput;
      addReplyInput(rest);
      dispatch(startSpinner());
      dispatch(replyPost(replyObj));
    }
  }

  const repliesRender = (replies) => {
    return replies.map(el => {
      return <div key={el._id} >
        <div className="post-comment reply-comments">
          <span className="post-comment-user">{el.user}</span>
          <span className="post-comment-msg">
            {el.reply}
            <div className="post-comment-reply">
              <span className="card_date">{getDateFormat(el.repliedAt)}</span>
            </div>
          </span>
        </div>
      </div>
    })
  }

  return (
    <>
      <div className={`${post.selectedFile === '' ? 'image_section empty_img' : 'image_section'}`}>
        <div className="card_header">
          <div className="card_header_left">
            <span className={`${post.selectedFile === '' ? 'card_title' : 'card_title_img'}`}>{post.creator}</span>
            <span className="card_date">
              {moment(post.createdAt).fromNow()}
            </span>
          </div>
          <div className="card_header_right">
            {options ?
              <>
                <img className="card_more_icon" alt="edit" src={Edit} />
                <img className="card_more_icon" alt="trash" src={Trash} onClick={() => dispatch(deletePost(post._id))} />
              </>
              :
              <img className="card_more_icon" alt="more" src={More} onClick={() => showOptions(true)} />}
          </div>
        </div>
        {post.selectedFile && <img className="card_image" alt="post_image" src={post.selectedFile} />}
      </div>
      <div className="card_message">{post.message}</div>
      <div className="card_likes">
        {post.likeCount.length}&nbsp;
        <img
          alt="like"
          src={post.likeCount.indexOf(user.lastName) > -1 ? Like_filled : Like}
          className="card_like_icon"
          onClick={() => { dispatch(likePost(post._id, user.lastName)); dispatch(startSpinner()) }}
        />
      </div>
      <div className="card_comments">
        {post.comments.length}&nbsp;
        <img
          alt="like"
          src={Chat}
          className="card_like_icon"
          onClick={() => toggleComments(s => !s)}
        />
      </div>
      <div className="comments-section">
        <Input
          type="text"
          placeholder="Enter comment..."
          styleName="comment-input"
          value={comment}
          onChange={(e) => handleComment(e.target.value)}
        />
        <Button value={"Comment"} styleName="comment-btn" clickHandler={commentHandler} />
      </div>
      {showComments && <div className="comments-wrapper">
        {post.comments.map((el, key) => {
          return <div className="comment-section" key={el._id} >
            <div className="post-comment">
              <span className="post-comment-user">{el.user}</span>
              <span className="post-comment-border" />
              <span className="post-comment-msg">
                {el.comment}
                <div className="post-comment-reply">
                  <span className="card-reply-btn" onClick={() => addReplyInput({ ...replyInput, [el._id]: '' })}>Reply,</span>
                  <span className="card_date">{getDateFormat(el.commentAt)}</span>
                </div>
              </span>
            </div>
            {
              replyInput[el._id] !== undefined &&
              <div key={el._id} className="post-comment new-reply-comment">
                <span className="post-comment-user">{el.user}</span>
                <div className="replyMsg-wrapper">
                  <span className="replyMsg-name">{el.user}</span>
                  <input
                    type="text"
                    placeholder="Enter Reply..."
                    className="post-reply-input"
                    value={replyInput[el._id]}
                    onChange={(e) => addReplyInput({ ...replyInput, [el._id]: e.target.value })}
                    onKeyPress={event => event.key === "Enter" && replyEnterHandler(el.user, el._id, key)} />
                </div>
              </div>
            }
            {el.replies.length > 0 && repliesRender(el.replies)}
          </div>
        })}
      </div>
      }
    </>
  );
};

export default Post;