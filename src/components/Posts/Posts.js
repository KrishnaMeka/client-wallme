import React from 'react';
import './Posts.scss';
import Post from './Post/Post';
import { useSelector } from 'react-redux';

const Posts = () => {
    const posts = useSelector((state) => state.posts);
    console.log("postsParen", posts);

    return (
        (
            posts &&
            <div className="postCard_Wrapper">
                {posts.map((post) => (
                    <div key={post._id} className="post_Card">
                        <Post post={post} />
                    </div>
                ))}
            </div>
        )
    );
};

export default Posts;