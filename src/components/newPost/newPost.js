import React, { useState, useEffect } from "react";
import InputField from "../../shared/inputField/inputField";
import Button from "../../shared/button/button";
import FileBase from "react-file-base64";
import { useDispatch } from "react-redux";
import { createPost } from "../../redux/actions/posts";
import { getPosts } from "../../redux/actions/posts";
import { useSelector } from 'react-redux';
import { startSpinner } from '../../redux/actions/spinnerAction';
import "./newPost.scss";

const NewPost = () => {
    const [formData, setFormData] = useState({
        message: "",
        selectedFile: ""
    });

    const dispatch = useDispatch();
    const loggedInUser = useSelector((state) => state.userInfo);
    console.log("loggedInUser", loggedInUser)
    useEffect(() => {
        dispatch(startSpinner());
        dispatch(getPosts());
    }, [dispatch]);

    const handleSubmit = (e) => {
        e.preventDefault();
        const newPost = {
            message: formData.message,
            selectedFile: formData.selectedFile,
            creator: loggedInUser.lastName || 'bobby'
        }
        dispatch(startSpinner());
        dispatch(createPost(newPost));
    };

    const clearForm = () => {
        setFormData({ message: "", selectedFile: "" });
    };

    return (
        <>
            <div className="moments-form">
                <form onSubmit={handleSubmit}>
                    <InputField
                        type="text"
                        placeholder={`What's your memory, ${loggedInUser.lastName || 'user'}?`}
                        styleName="form-textfield"
                        value={formData.message}
                        onChange={(e) =>
                            setFormData({ ...formData, message: e.target.value })
                        }
                    />
                    <div className="fileUpload moments-fileUpload">
                        <FileBase
                            className="moments-inputFile"
                            type="file"
                            multiple={false}
                            onDone={({ base64 }) =>
                                setFormData({ ...formData, selectedFile: base64 })
                            }
                        />
                    </div>
                    <div className="form-btns">
                        <Button value={"Post"} type="submit" styleName="form-post-btn" />
                        <Button value={"Clear"} clickHandler={clearForm} styleName="form-clear-btn" />
                    </div>
                </form>
            </div>
        </>
    );
};

export default NewPost;
