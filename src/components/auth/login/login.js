import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux';
import Button from "../../../shared/button/button";
import FormFields from '../../../shared/formFields/formFields';
import PictureUpload from "../../../shared/pictureUpload/pictureUpload";
import { loginUser } from '../../../redux/actions/userAuth';
import { startSpinner, stopSpinner } from '../../../redux/actions/spinnerAction';
import * as api from '../../../api/index';
import "./login.scss";

const Login = () => {
    const [loginFields, setLoginFields] = useState({
        loginEmail: "",
        loginPassword: "",
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        confirmPassword: "",
        createAccount: false,
        loginInfo: "",
        imageUpload: true,
        base64Image: "",
        imageFile: null
    });
    const dispatch = useDispatch();
    const userInfo = useSelector((state) => state.userInfo);

    useEffect(() => {
        if (userInfo.message === "Invalid Email or Password") {
            console.log("logintest", userInfo)
            setLoginFields({
                ...loginFields, loginInfo: userInfo.message
            })
        }
    }, [loginFields, userInfo, dispatch]);

    const loginUserHandler = async () => {
        if (loginFields.loginEmail !== '' && loginFields.loginPassword !== '') {
            const obj = {
                email: loginFields.loginEmail,
                password: loginFields.loginPassword
            }
            try {
                dispatch(startSpinner());
                const { data } = await api.loginUser(obj);
                console.log("loginActionData", data);
                dispatch(loginUser(data));

            } catch (error) {
                dispatch(stopSpinner());
                console.log("loginActionDataErr", error, error.response);
                if (error.response) {
                    const { data } = error.response;
                    setLoginFields({
                        ...loginFields, loginInfo: data.message
                    })

                }
            }
        }
        else {
            setLoginFields({ ...loginFields, loginInfo: 'Please enter valid Email and Password' });
        }
    }

    const submitAccount = async () => {
        console.log("userCreatedSubmi")

        const { email, password, confirmPassword, firstName, lastName, base64Image } = loginFields;

        if (email !== "" && email.indexOf('@') === -1) {
            setLoginFields({ ...loginFields, loginInfo: 'Please enter valid email ID' });
        }
        else if (password !== confirmPassword) {
            setLoginFields(() => ({ ...loginFields, loginInfo: 'Password and confirm password does not match' }));
        }
        else {
            try {
                const createObj = {
                    email, password, firstName, lastName, profileDp: base64Image
                }
                dispatch(startSpinner());
                const { data } = await api.createUser(createObj);
                console.log("createUserData", data);
                dispatch(stopSpinner());
                if (data.message === "User registered Successfully") {
                    setLoginFields({
                        loginEmail: "",
                        loginPassword: "",
                        firstName: "",
                        lastName: "",
                        email: "",
                        password: "",
                        confirmPassword: "",
                        createAccount: false,
                        loginInfo: "",
                        imageUpload: true,
                        base64Image: "",
                        imageFile: null
                    })
                }
            } catch (error) {
                dispatch(stopSpinner());
                console.log("createUserDataErr", error, error.response)
                if (error.response) {
                    const { data } = error.response;
                    console.log("createUserDataErrIn", data)
                    setLoginFields({
                        ...loginFields, loginInfo: data.message
                    })
                }
            }
        }
    }

    return (
        <div className="loginWrapper">
            <div className="loginTitleSection">
                <span className="loginTitle">Moments</span>
                <span className="loginSubTitle">
                    {loginFields.createAccount ? "Sign Up" : "Sign In"}
                </span>
            </div>
            {!loginFields.createAccount && (
                <div className="loginSection">
                    <div className="loginFields">
                        <FormFields
                            type="email"
                            name="loginEmail"
                            className="moment-inputField"
                            label="Email"
                            value={loginFields.loginEmail}
                            formHandler={(val, name) => setLoginFields({ ...loginFields, [name]: val, loginInfo: "" })}
                        />
                        <FormFields
                            type="password"
                            name="loginPassword"
                            className="moment-inputField"
                            label="Password"
                            value={loginFields.loginPassword}
                            formHandler={(val, name) => setLoginFields({ ...loginFields, [name]: val, loginInfo: "" })}
                        />
                    </div>
                    <span className="createLink" onClick={() => setLoginFields({ ...loginFields, createAccount: true, imageUpload: false })}>
                        Create Account
                    </span>
                    <Button
                        clickHandler={loginUserHandler}
                        styleName="signInBtn"
                        value="Log In"
                    />
                </div>
            )}
            {loginFields.createAccount && (
                <div className="signinSection">
                    <div className="loginFields">
                        <FormFields
                            type="text"
                            name="firstName"
                            className="moment-inputField"
                            label="First Name"
                            value={loginFields.firstName}
                            formHandler={(val, name) => setLoginFields({ ...loginFields, [name]: val })}
                        />
                        <FormFields
                            type="text"
                            name="lastName"
                            className="moment-inputField"
                            label="Last Name"
                            value={loginFields.lastName}
                            formHandler={(val, name) => setLoginFields({ ...loginFields, [name]: val })}
                        />
                        <FormFields
                            type="email"
                            name="email"
                            className="moment-inputField"
                            label="Email"
                            value={loginFields.email}
                            formHandler={(val, name) => setLoginFields({ ...loginFields, [name]: val })}
                        />
                        <FormFields
                            type="password"
                            name="password"
                            className="moment-inputField"
                            label="Password"
                            value={loginFields.password}
                            formHandler={(val, name) => setLoginFields({ ...loginFields, [name]: val })}
                        />
                        <FormFields
                            type="password"
                            name="confirmPassword"
                            className="moment-inputField"
                            label="Confirm Password"
                            value={loginFields.confirmPassword}
                            formHandler={(val, name) => setLoginFields({ ...loginFields, [name]: val })}
                        />
                        <div className="picture-uploadSection">
                            <div className="picture-uploadLink" onClick={() => setLoginFields({ ...loginFields, imageUpload: true })}>
                                Click here to upload a profile picture
                            </div>
                            <div className="picture-UploadName">
                                Profile Pictue:{" "}
                                {loginFields.imageFile
                                    ? loginFields.imageFile.name
                                    : "Not Uploaded"}
                            </div>
                        </div>
                        <Button clickHandler={submitAccount} styleName="signInBtn" value="Create Account" />
                        {loginFields.imageUpload && (
                            <PictureUpload
                                createAccount={true}
                                imageFiles={(imageFile, base64Image) => setLoginFields({ ...loginFields, imageFile, base64Image, imageUpload: false })}
                                closeUploader={() => setLoginFields({ ...loginFields, imageUpload: false })}
                            />
                        )}
                    </div>
                </div>
            )}
            <div className="loginError">{loginFields.loginInfo}</div>
        </div>
    )
}

export default Login;