import React, { useState, useEffect, useRef } from 'react';
import { withRouter } from 'react-router-dom';
import './profile.scss';
import FormValidation from '../../assets/validations/formValidations';
import FormFields from '../../shared/formFields/formFields';
import Button from '../../shared/button/button';
import { useSelector, useDispatch } from 'react-redux';
import { updateUser } from '../../redux/actions/userAuth';

const Profile = (props) => {
    const [formFields, setFormFields] = useState({
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        confirmPassword: ''
    });
    const [errorFields, setErrorFields] = useState({});
    const [editForm, setEditForm] = useState(false);
    const [passwordChange, passChangeHandler] = useState(false);
    const [submitError, changeSubmitError] = useState('');
    const userInfo = useSelector((state) => state.userInfo);
    const dispatch = useDispatch();
    const prevUser = useRef(userInfo).current;

    const updateInitialState = () => {
        const { firstName, lastName, email } = userInfo;
        setFormFields({ ...formFields, firstName: firstName, lastName: lastName, email: email, password: '', confirmPassword: '' });
        setErrorFields({});
        changeSubmitError('');
        setEditForm(false);
    };

    useEffect(() => {
        updateInitialState()
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    useEffect(() => {
        if (prevUser.lastName !== userInfo.lastName ||
            prevUser.firstName !== userInfo.firstName ||
            prevUser.email !== userInfo.email
        ) {
            props.history.replace('./')
        }
    }, [userInfo]); // eslint-disable-line react-hooks/exhaustive-deps

    const formInputHandler = (value, name) => {
        let error;
        if (name !== 'confirmPassword') {
            error = FormValidation({ [name]: value });
        } else {
            error = FormValidation({ [name]: value, password: formFields.password });
        }

        if (error[name] === undefined) {
            delete errorFields[name];
            setErrorFields(errorFields);
        } else {
            setErrorFields({ ...errorFields, [name]: error[name] });
        }
        changeSubmitError('');
        setFormFields({ ...formFields, [name]: value });
    }

    const passwordHandler = () => {
        if (passwordChange) {
            setFormFields({ ...formFields, password: '', confirmPassword: '' });
            changeSubmitError('');
        }
        passChangeHandler(s => !s);
    }

    const submitHandler = () => {
        const { firstName, lastName, email, password, confirmPassword } = formFields;
        if (Object.keys(errorFields).length > 0) {
            changeSubmitError('Please correct errors and submit again.')
        } else if (firstName === '' || lastName === '' || email === '') {
            changeSubmitError('Fields cannot be empty.')
        } else if (passwordChange && password === '') {
            changeSubmitError('Password cannot be empty. If you do not wish to submit password, please click cancel password change')
        } else if (passwordChange && password !== '' && confirmPassword === '') {
            changeSubmitError('Confirm Password cannot be empty. If you do not wish to submit password, please click cancel password change')
        } else {
            let updatedUser = Object.assign({}, formFields);
            updatedUser.id = userInfo.id;
            dispatch(updateUser(updatedUser));
        }
    }

    return (
        <div className="profile-wrapper">
            <section className="profile-form">
                <div className="profile-form-img">
                    <img alt="dp" src={userInfo.profileDp} />
                </div>
                <form>
                    <FormFields
                        type="text"
                        disabled={!editForm}
                        name="firstName"
                        label="First Name"
                        placeholder="First Name"
                        value={formFields.firstName}
                        formHandler={formInputHandler}
                        error={errorFields.firstName}
                    />
                    <FormFields
                        type="text"
                        disabled={!editForm}
                        name="lastName"
                        label="Last Name"
                        placeholder="Last Name"
                        value={formFields.lastName}
                        formHandler={formInputHandler}
                        error={errorFields.lastName}
                    />

                    <FormFields
                        type="text"
                        name="email"
                        label="Email"
                        disabled={!editForm}
                        placeholder="Email"
                        value={formFields.email}
                        formHandler={formInputHandler}
                        error={errorFields.email}
                    />
                    {editForm &&
                        <div className="password-wrapper">
                            <div
                                className="password-msg"
                                onClick={passwordHandler}>
                                {passwordChange ? 'Cancel Password Change' : 'Change Password'}
                            </div>

                            {
                                editForm && passwordChange &&
                                <>
                                    <FormFields
                                        type="text"
                                        name="password"
                                        label="Password"
                                        placeholder="password"
                                        value={formFields.password}
                                        formHandler={formInputHandler}
                                        error={errorFields.password}
                                    />
                                    <FormFields
                                        type="text"
                                        name="confirmPassword"
                                        label="Confirm Password"
                                        placeholder="confirmPassword"
                                        value={formFields.confirmPassword}
                                        formHandler={formInputHandler}
                                        error={errorFields.confirmPassword}
                                    />
                                </>
                            }
                        </div>

                    }
                </form>
                <div className="submitErrors">{submitError}</div>
                <div className="profile-btns-wrapper">
                    {!editForm && <Button value={"Edit Profile"} clickHandler={() => setEditForm(true)} styleName="profie-btns" />}
                    {editForm && <Button value={"Submit"} clickHandler={submitHandler} styleName="profie-btns" />}
                    {editForm && <Button value={"Cancel"} clickHandler={updateInitialState} styleName="profie-btns" />}
                </div>
            </section>
        </div >
    )
}

export default withRouter(Profile);