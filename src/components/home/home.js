import React from "react";
import Posts from "../Posts/Posts";
import NewPost from '../newPost/newPost'
import "./home.scss";

const Form = () => {
  return (
    <>
      <div className="homePage-wrapper">
        <NewPost />
        <Posts />
      </div>
    </>
  );
};

export default Form;
