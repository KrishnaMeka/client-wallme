import React, { useState } from 'react';
import './header.scss';
import dp from '../../assets/images/person.png';
import { NavLink } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { logoutUser } from '../../redux/actions/userAuth';

const Navbar = (props) => {

    const dispatch = useDispatch();
    const [signOut, setSignOut] = useState(false);
    const userInfo = useSelector((state) => state.userInfo);

    return (
        <>
            <div className="headerWrapper">
                <div className="headerItems-left">
                    <img className="header-dp" src={userInfo.profileDp || dp} alt="pic" />
                </div>
                <div className="app-title">
                    <NavLink to="/" exact={true} className="appLinks">Home</NavLink>
                    <NavLink to="/news" className="appLinks">News</NavLink>
                    <NavLink to="/profile" className="appLinks">Profile</NavLink>
                </div>
                <div className="navbar_border" />
                <div className="headerItems-right">
                    <div className="userName-display">{userInfo.lastName || ''}</div>
                    <div className="username-iconWrapper" onClick={() => setSignOut(s => !s)} >
                        <span className={`username-icon ${signOut ? 'arrow-up' : 'arrow-down'}`} />
                    </div>
                    {signOut &&
                        <ul className="userDropdown">
                            <li className="userDropdownItem" onClick={() => dispatch(logoutUser())}>Log Out</li>
                        </ul>
                    }
                </div>
            </div>

        </>

    )

}

export default Navbar;