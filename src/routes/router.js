import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "../components/home/home";
import Header from '../components/header/header';
import Profile from '../components/profile/profile';
import '../assets/styles/globalStyles.scss';
import Spinner from '../shared/spinner/spinner';

const AppRouter = () => {
  return (
    <>
      <Header />
      <Spinner />
      <div className="moments-wrapper">
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/profile" component={Profile} />
        </Switch>
      </div>
    </>
  );
};

export default AppRouter;
