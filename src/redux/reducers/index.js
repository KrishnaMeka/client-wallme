import { combineReducers } from 'redux';
import posts from './posts';
import userInfo from './userInfo';
import globalSpinner from './spinnerReducer';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

const reducers = combineReducers({
    posts,
    userInfo,
    globalSpinner
})

export default createStore(reducers, compose(applyMiddleware(thunk)));