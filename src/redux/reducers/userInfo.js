import { USER_INFO, UPDATE_USER, LOGOUT_USER } from '../constants/actionTypes';

// const initialState = {
//     email: "john@gmail.com",
//     firstName: "John",
//     isAdmin: false,
//     lastName: "Nathan",
//     profileDp: "",
//     id: "601840d65c260630b40b5120"
// }

const initialState = {
    email: "",
    firstName: "",
    isAdmin: false,
    lastName: "",
    profileDp: "",
    id: ""
}

const loginUser = (user = initialState, action) => {
    console.log("userInfoReducer", user, action.type, initialState)
    switch (action.type) {
        case USER_INFO:
            return {
                ...user, ...action.payload
            }
        case UPDATE_USER:
            return {
                ...user,
                ...action.payload
            }
        case LOGOUT_USER:
            return {
                ...user,
                ...initialState
            }
        default: return user
    }
}

export default loginUser;