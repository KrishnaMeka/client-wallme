import { FETCH_POSTS, CREATE_POST, DELETE_POST, LIKE_POST, COMMENT_POST, REPLY_POST } from "../constants/actionTypes";

const posts = (posts = [], action) => {
  console.log("reduxPosts", posts);
  if (action.type === LIKE_POST) {
    console.log("reduxPostsIns", action.payload);
  }
  switch (action.type) {
    case FETCH_POSTS:
      return action.payload;
    case CREATE_POST:
      return [action.payload, ...posts];
    case DELETE_POST:
      return posts.filter((post) => post._id !== action.payload);
    case LIKE_POST:
      return posts.map((post) => post._id === action.payload._id ? action.payload : post);
    case COMMENT_POST:
      return posts.map((post) => post._id === action.payload._id ? action.payload : post);
    case REPLY_POST:
      return posts.map((post) => post._id === action.payload._id ? action.payload : post);
    default:
      return posts;
  }
};

export default posts;
