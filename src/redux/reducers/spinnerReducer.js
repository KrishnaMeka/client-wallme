import { START_SPINNER, STOP_SPINNER } from '../constants/actionTypes';

const globalSpinner = (state = false, action) => {
    console.log("spinnerReducer", state, action.type)
    switch (action.type) {
        case START_SPINNER:
            return true
        case STOP_SPINNER:
            return false
        default: return state
    }
}

export default globalSpinner;