import { START_SPINNER, STOP_SPINNER } from '../constants/actionTypes';

export const startSpinner = () => dispatch => {
    dispatch({ type: START_SPINNER })
}

export const stopSpinner = () => dispatch => {
    dispatch({ type: STOP_SPINNER })
}