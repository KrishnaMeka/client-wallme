import * as api from '../../api/index';
import { USER_INFO, UPDATE_USER, LOGOUT_USER, STOP_SPINNER } from '../constants/actionTypes';

export const loginUser = (user) => {
    return ({ type: USER_INFO, payload: user });
}

export const updateUser = (user) => async (dispatch) => {
    try {
        const { data } = await api.updateUser(user)
        console.log("updateAction", data)
        dispatch({ type: UPDATE_USER, payload: data });
        dispatch({ type: STOP_SPINNER });
    } catch (error) {
        dispatch({ type: STOP_SPINNER });
        console.log("updateActionErr", error, error.message)
    }
}

export const logoutUser = () => {
    return ({ type: LOGOUT_USER });
}