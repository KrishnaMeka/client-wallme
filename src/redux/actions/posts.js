import * as api from "../../api/index";
import { FETCH_POSTS, CREATE_POST, DELETE_POST, LIKE_POST, COMMENT_POST, REPLY_POST, STOP_SPINNER } from "../constants/actionTypes";

export const getPosts = () => async (dispatch) => {
  try {
    const { data } = await api.fetchPosts();
    dispatch({ type: FETCH_POSTS, payload: data });
    dispatch({ type: STOP_SPINNER });
  } catch (error) {
    dispatch({ type: STOP_SPINNER });
    console.log("fetch_posts", error.message);
  }
};

export const createPost = (post) => async (dispatch) => {
  try {
    const { data } = await api.createPost(post);
    dispatch({ type: CREATE_POST, payload: data });
    dispatch({ type: STOP_SPINNER });
  } catch (error) {
    dispatch({ type: STOP_SPINNER });
    console.log("crate_posts", error.message);
  }
};

export const deletePost = (id) => async (dispatch) => {
  try {
    await api.deletePost(id);
    dispatch({ type: DELETE_POST, payload: id });
    dispatch({ type: STOP_SPINNER });
  } catch (error) {
    dispatch({ type: STOP_SPINNER });
    console.log("deleteResErr", error.message);
  }
}

export const likePost = (id, name) => async (dispatch) => {
  try {
    const { data } = await api.likePost(id, name);
    dispatch({ type: LIKE_POST, payload: data });
    dispatch({ type: STOP_SPINNER });
  }
  catch (error) {
    dispatch({ type: STOP_SPINNER });
    console.log("actionLikeErr", error)
  }
}

export const commentPost = (value) => async (dispatch) => {
  try {
    const { data } = await api.commentPost(value);
    dispatch({ type: COMMENT_POST, payload: data });
    dispatch({ type: STOP_SPINNER });
  } catch (error) {
    dispatch({ type: STOP_SPINNER });
    console.log("commentResErr", error, error.message)
  }
}

export const replyPost = (reply) => async (dispatch) => {
  try {
    const { data } = await api.replyPost(reply);
    dispatch({ type: REPLY_POST, payload: data });
    dispatch({ type: STOP_SPINNER });
  } catch (error) {
    dispatch({ type: STOP_SPINNER });
    console.log("actionReplyErr", error, error.message);
  }
}