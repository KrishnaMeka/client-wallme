import axios from 'axios';

const url = 'http://localhost:5000/';


export const loginUser = (user) => axios.post(`${url}user/login`, user);
export const createUser = (newUser) => axios.post(`${url}user/register`, newUser);
export const updateUser = (user) => axios.put(`${url}user/updateUser`, user);
export const createPost = (newPost) => axios.post(`${url}posts`, newPost);
export const fetchPosts = () => axios.get(`${url}posts`);
export const deletePost = (id) => axios.delete(`${url}posts/${id}`);
export const likePost = (id, name) => axios.put(`${url}posts/${id}/${name}`);
export const commentPost = (value) => axios.put(`${url}posts/comment`, value);
export const replyPost = (reply) => axios.put(`${url}posts/reply`, reply);